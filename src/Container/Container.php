<?php

namespace Container;

use ArrayAccess;
use Closure;
use ReflectionClass;
use ReflectionParameter;
use Psr\Container\ContainerInterface;

class Container implements ArrayAccess, ContainerInterface
{
    /**
     * @var array
     */
    protected $values = [];
    
    /**
     * @param string $binding
     * @return mixed
     */
    public function resolve(string $binding)
    {
        $concrete = $this->values[$binding] ?? $binding;
        
        return $this->build($concrete);
    }

    /**
     * @param string|Closure $concrete
     * @return mixed
     */
    public function build($concrete)
    {
        if ($concrete instanceof Closure) {
            return $concrete();
        }

        $reflector = new ReflectionClass($concrete);

        if (!$reflector->isInstantiable()) {
            throw new ContainerException;
        }

        $constructor = $reflector->getConstructor();

        if (is_null($constructor)) {
            return new $concrete;
        }

        $dependencies = $constructor->getParameters();

        $instances = $this->resolveDependencies($dependencies);

        return $reflector->newInstanceArgs($instances);
    }

    /**
     * Resolve parameter dependencies.
     *
     * @param array $parameters
     * @return array
     */
    protected function resolveDependencies(array $parameters)
    {
        $dependencies = [];

        foreach ($parameters as $parameter) {
            // If we return null, then we can assume its a primitive that we'll 
            // try to resolve. Otherwise, we will try to resolve the class.
            if (is_null($parameter->getClass())) {
                $dependencies[] = $this->resolvePrimitive($parameter);
            } else {
                $dependencies[] = $this->resolveClass($parameter);
            }
        }

        return $dependencies;
    }
    
    /**
     * @param ReflectionParameter $parameter
     * @return object
     */
    public function resolveClass(ReflectionParameter $parameter)
    {
        return $this->resolve($parameter->getClass()->name);
    }
    
    /**
     * @param ReflectionParameter $parameter
     * @return mixed
     */
    public function resolvePrimitive(ReflectionParameter $parameter)
    {
        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }
        
        throw new \Exception(
            sprintf('Unresolvable primitive [$%s] in class [%s]', $parameter->name, $parameter->getDeclaringClass()->name)
        );
    }
    
    /**
     * {@inheritDoc}
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->resolve($id);
        }
        
        throw new NotFoundException;
    }
    
    /**
     * {@inheritDoc}
     */
    public function has($id)
    {
        return isset($this->values[$id]);
    }

    /**
     * [offsetExists description]
     *
     * @param string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->values);
    }

    /**
     * [offsetGet description]
     *
     * @param string $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->resolve($offset);
    }

    /**
     * [offsetSet description]
     *
     * @param string $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->values[$offset] = $value;
    }

    /**
     * [offsetUnset description]
     *
     * @param string $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->values[$offset]);
        }
    }
}