<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Container\Container;

class ContainerTest extends TestCase
{
    public function testParamDefault()
    {
        $container = new Container;
        $class = $container->build(ContainerParamDefaultStub::class);
        
        $this->assertEquals('Foo', $class->name);
    }
    
    public function testParamConcrete()
    {
        $container = new Container;
        $class = $container->build(ContainerParamConcreteStub::class);
        
        $this->assertInstanceOf(ConcreteStub::class, $class->concrete);
    }
}

class ContainerParamDefaultStub
{
    public $name;
    public function __construct($name = 'Foo')
    {
        $this->name = $name;
    }
}

class ContainerParamConcreteStub
{
    public $concrete;
    public function __construct(ConcreteStub $concrete)
    {
        $this->concrete = $concrete;
    }
}

class ConcreteStub
{
    //
}
